$URI = "https://www.vpnme.me/freevpn.html"
$HTML = Invoke-WebRequest -Uri $URI
$Password = @(@(@($HTML.ParsedHtml.getElementsByTagName("TABLE"))[1].rows)[2].cells)[2].innerText

$CheminPasswordFile = $PSScriptRoot + "\pass.txt"

"fr-open" | Out-File -Encoding Default -FilePath $CheminPasswordFile
$Password | Out-File -Encoding Default -FilePath $CheminPasswordFile -Append 

cd $PSScriptRoot
& "openvpn" vpnme_fr_tcp443.ovpn