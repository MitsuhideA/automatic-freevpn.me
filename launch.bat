@ECHO OFF
SET ThisScriptsDirectory=%~dp0
SET ScriptPowerShell=script.ps1
SET PowerShellScriptPath=%ThisScriptsDirectory%%ScriptPowerShell%
PowerShell -NoProfile -ExecutionPolicy Bypass -Command "& {Start-Process PowerShell -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File ""%PowerShellScriptPath%""' -Verb RunAs}";