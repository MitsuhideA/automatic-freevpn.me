# Comment débloquer le WiFi de merde de PolyTech ? #

## Installation ! ##
* Installer OpenVpn : https://openvpn.net/index.php/open-source/downloads.html (Installer (64-bit), Windows Vista and later)
* Installer les mises à jour obligatoires de Windows (dans l'ordre!) (si t'as Win10 pas besoin) :
    * https://bitbucket.org/MitsuhideA/automatic-freevpn.me/downloads/dotNetFx40_Full_setup.exe
    * https://bitbucket.org/MitsuhideA/automatic-freevpn.me/downloads/Windows6.1-KB2506143-x64.msu
* Redémarrer le Pc (si t'as Win10 pas besoin) !

## Préparation ! ##

* Télécharger le Zip : https://bitbucket.org/MitsuhideA/automatic-freevpn.me/get/master.zip. Ancienne version (https://bitbucket.org/MitsuhideA/automatic-freevpn.me/get/freevpnme.zip)
* Décompresser où tu veux !

## Utilisation ! ##
* Il faut juste lancer launch.bat
* Clique sur oui pour le PowerShell.
* Plein de texte qui défile !
* Si t'as "Initialization Sequence Completed" à la fin, c'est connecté ! (ne pas fermer la console pour garder le déblocage)